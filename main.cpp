#include <dpp/dpp.h>
#include <fmt/core.h>

#include <iostream>
#include <nlohmann/json.hpp>
#include <random>

#include "secrets.h"

struct DictionaryEntry {
  std::string              word;
  std::vector<std::string> definitions;
};

class Random {
 public:
  explicit Random(size_t max) : distribution(0, max - 1) { }
  size_t getNext() { return distribution(rd); }

 private:
  std::random_device                    rd;
  std::uniform_int_distribution<size_t> distribution;
};

class DefinitionGame {
 public:
  struct Command {
    static constexpr const char *reset             = "reset";
    static constexpr const char *join              = "join";
    static constexpr const char *next              = "next";
    static constexpr const char *reveal            = "reveal";
    static constexpr const char *answer            = "answer";
    static constexpr const char *answer_definition = "definition";
    static constexpr const char *dictionary        = "dictionary";
    static constexpr const char *dictionary_index  = "index";

    static bool isGameContextual(const std::string &command) {
      return (command == next || command == reveal || command == answer);
    }
  };

 public:
  void addPlayer(const dpp::user &user) { mPlayers.push_back(user.username); }

  [[nodiscard]] bool hasPlayer(const dpp::user &user) const {
    for (const auto &player : mPlayers) {
      if (player == user.username) return true;
    }
    return false;
  }

  void handleCommand(const dpp::slashcommand_t &event) {
    if (!hasPlayer(event.command.usr)) {
      event.reply("Not a member of the game, use /join to join the game");
    } else if (event.command.get_command_name() == Command::next) {
      handleNext(event);
    } else if (event.command.get_command_name() == Command::reveal) {
      handleReveal(event);
    } else if (event.command.get_command_name() == Command::answer) {
      handleAnswer(event);
    }
  }

  void reset() {
    mRandom = std::make_unique<Random>(sDictionary.size());
    mPlayers.clear();
    mAnswers.clear();
  }

  static void loadDefinitions() {
    std::ifstream  i("dictionary.json");
    nlohmann::json j;
    i >> j;
    for (const auto &index : j) {
      DictionaryEntry entry;
      entry.word = index["word"];
      for (const auto &definition : index["definitions"]) {
        entry.definitions.push_back(definition.get<std::string>());
      }
      sDictionary.push_back(std::move(entry));
    }
    std::cout << "definitions loaded" << std::endl;
  }

  static void handleDictionary(const dpp::slashcommand_t &event) {
    auto parameter = event.get_parameter(Command::dictionary_index);
    if (std::holds_alternative<long>(parameter)) {
      long index = std::get<long>(parameter);
      if (index > sDictionary.size() || index < 1) {
        event.reply(fmt::format("Invalid index. Max index is {}", sDictionary.size()));
      } else {
        event.reply(fmt::format(
            "## {}\n{}",
            sDictionary[index - 1].word,
            formatDefinitions(sDictionary[index - 1].definitions)));
      }
    } else {
      event.reply(fmt::format("Dictionary holds {} words", sDictionary.size()));
    }
  }

 private:
  static std::string formatDefinitions(const std::vector<std::string> &definitions) {
    std::stringstream ss;
    for (int i = 0; i < definitions.size(); ++i) { ss << i + 1 << ". " << definitions[i] << "\n"; }
    return ss.str();
  }

  void handleNext(const dpp::slashcommand_t &event) {
    mAnswers.clear();
    sDictionary.size();
    mIndex = mRandom->getNext();
    event.reply(fmt::format("New word: {}", sDictionary[mIndex].word));
  }

  void handleReveal(const dpp::slashcommand_t &event) {
    event.reply(fmt::format(
        "# {}\n{}\n{}",
        sDictionary[mIndex].word,
        formatDefinitions(sDictionary[mIndex].definitions),
        formatAnswers(mAnswers)));
  }

  void handleAnswer(const dpp::slashcommand_t &event) {
    mAnswers[event.command.usr.username] = std::get<std::string>(event.get_parameter(Command::answer_definition));
    event.reply(fmt::format(
        "{} has answered. {} players remaining",
        event.command.usr.global_name,
        mPlayers.size() - mAnswers.size()));
  }

  static std::string formatAnswers(const std::unordered_map<std::string, std::string> &answers) {
    std::stringstream ss;
    ss << "## Réponses des participants\n";
    for (const auto &[user, answer] : answers) { ss << "### " << user << "\n" << answer << "\n\n"; }
    return ss.str();
  }

 private:
  static std::vector<DictionaryEntry>          sDictionary;
  std::unique_ptr<Random>                      mRandom;
  size_t                                       mIndex   = 0;
  std::unordered_map<std::string, std::string> mAnswers = std::unordered_map<std::string, std::string>();
  std::vector<std::string>                     mPlayers;
};

class DefinitionGameManager {
 public:
  static void handleCommand(const dpp::slashcommand_t &event) {
    if (DefinitionGame::Command::isGameContextual(event.command.get_command_name())) {
      mGame.handleCommand(event);
    } else if (event.command.get_command_name() == DefinitionGame::Command::reset) {
      mGame.reset();
      event.reply(fmt::format("New game created. Use /join to rejoin"));
    } else if (event.command.get_command_name() == DefinitionGame::Command::join) {
      mGame.addPlayer(event.command.usr);
      event.reply(fmt::format("{} has joined the party. wooh!", event.command.usr.username));
    } else if (event.command.get_command_name() == DefinitionGame::Command::dictionary) {
      DefinitionGame::handleDictionary(event);
    } else {
      event.cancel_event();
    }
  }

  static void registerCommands(dpp::cluster &bot) {
    auto answer = dpp::slashcommand(DefinitionGame::Command::answer, "Provide your answer for this round", bot.me.id);
    answer.add_option(dpp::command_option(
        dpp::command_option_type::co_string,
        DefinitionGame::Command::answer_definition,
        "The word definition",
        true));

    auto dictionary = dpp::slashcommand(
        DefinitionGame::Command::dictionary,
        "Show the number of words stored in the dictionary",
        bot.me.id);
    dictionary.add_option(dpp::command_option(
        dpp::command_option_type::co_integer,
        DefinitionGame::Command::dictionary_index,
        "show the definitions of a word",
        false));

    bot.global_bulk_command_create({
        dpp::slashcommand(DefinitionGame::Command::reset, "Reset the game", bot.me.id),
        dpp::slashcommand(DefinitionGame::Command::join, "Join the game", bot.me.id),
        dpp::slashcommand(DefinitionGame::Command::next, "Request the next word", bot.me.id),
        dpp::slashcommand(DefinitionGame::Command::reveal, "Reveal each participant answers", bot.me.id),
        answer,
        dictionary,
    });
  }

  static void loadDefinitions() { DefinitionGame::loadDefinitions(); }

 private:
  static DefinitionGame mGame;
  static size_t         mGameIndexCounter;
};

std::vector<DictionaryEntry> DefinitionGame::sDictionary              = {};
DefinitionGame               DefinitionGameManager::mGame             = DefinitionGame();
size_t                       DefinitionGameManager::mGameIndexCounter = 0;

int main() {
  dpp::cluster bot(BOT_TOKEN);

  bot.on_log(dpp::utility::cout_logger());

  bot.on_slashcommand([](const dpp::slashcommand_t &event) { DefinitionGameManager::handleCommand(event); });

  bot.on_ready([&bot](const dpp::ready_t &event) {
    if (dpp::run_once<struct register_bot_commands>()) {
      DefinitionGameManager::registerCommands(bot);
      DefinitionGameManager::loadDefinitions();
    }
  });

  bot.start(dpp::st_wait);
}
